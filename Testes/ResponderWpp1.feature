Feature: Fun��o responder deve "printar" a mensagem a ser respondida acima da resposta enviada. A mensagem printada deve ter sido recebida, n�o enviada.

Contexto:
Dado que estou logado no Whatsapp
E estou com a tela de Conversa com outro usuario do app aberta
E que este outro usuario j� tenha me enviado uma mensagem

Cenario:
Quando dou um clique duplo em uma mensagem
E seleciono a op��o Responder
Ent�o devo visualizar em uma nova caixa de texto o nome deste usu�rio
E devo ver a mensagem que escolhi responder abaixo do nome do usu�rio 
E devo ver um icone indicando que tenho a op�ao de cancelar a a�ao dentro desta mesma caixa

