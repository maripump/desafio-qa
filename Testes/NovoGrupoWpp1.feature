Feature: Devo Visualizar a tela Adicionar participantes

Contexto:
Dado que estou logado no Whatsapp
E tenha contatos adicionados

Cenario:
Quando clico em Novo Grupo
Entao devo visualizar a tela de Adicionar Participantes
E todos os contatos devem possuir uma check box disponivel para ser marcada
E o bot�o Seguinte deve estar desabilitado
E o bot�o Cancelar deve estar habilitado
E a caixa de texto Buscar habilitada