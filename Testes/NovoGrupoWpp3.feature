Feature: Devo visualizar a tela Novo Grupo

Contexto:
Dado que selecionei um contato na tela Adicionar Participantes
E o bot�o Seguinte foi habilitado

Cenario:
Quando clico no bot�o Seguinte
Ent�o devo visualizar a tela Novo Grupo
E a caixa de texto Nome do Grupo habilitada
E o �cone Editar Foto habilitado
E a lista de contatos que selecionei na tela anterior
E o bot�o Criar desabilitado
E o bot�o Voltar habilitado