Feature: A Resposta Direta de uma mensagem recebida deve ser cancelada.

Contexto:
Dado que cliquei em Responder
E a mensagem a ser respondida foi impressa acima da caixa de texto de mensagens a enviar

Cenario:
Quando clico no �cone de cancelar da mensagem impressa
Entao a impressao da mensagem � retirada
E a tela volta ao layout original
