Feature: A mensagem recebida e sua resposta devem aparecer no label de mensagens enviadas e recebidas da tela de Conversa.

Contexto:
Dado que cliquei em Responder
E a mensagem a ser respondida foi impressa acima da caixa de texto de mensagens a enviar

Quando eu escrever uma mensagem na caixa de texto
E clicar no icone de enviar
Entao devo ver na caixa de mensagens enviadas e recebidas uma foto da mensagem que escolhi responder
E a minha resposta impressa abaixo desta foto