Feature: Novo Grupo � criado.

Contexto:
Dado que o bot�o Criar da tela Novo Grupo foi habilitado

Cenario:
Quando clico em Criar
Entao devo visualizar a tela de Conversa
E o Nome do Grupo que inseri deve ser impresso no topo da tela
E os nomes dos Contatos que foram adicionados ao grupo devem ser impressos abaixo do Nome do Grupo